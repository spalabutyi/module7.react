import Certificates from "../pages/Certificates";
import About from "../pages/About";
import Error from "../pages/Error";
import Login from "../pages/Login";

export const privateRoutes = [
  { path: "/about", element: <About />, exact: true },
  { path: "/certificates", element: <Certificates />, exact: true },
  { path: "*", element: <Error />, exact: true },
];

export const publicRoutes = [
  { path: "/login", element: <Login />, exact: true },
  { path: "*", element: <Login />, exact: true },
];
