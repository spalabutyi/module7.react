import {BrowserRouter} from "react-router-dom";
import {useEffect, useState} from "react";
import {AuthContext} from "./context";
import Navbar from "./UI/Navbar/Navbar";
import AppRouter from "./components/AppRouter";
import "./App.css";

function App() {
    const [isAuth, setIsAuth] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [username, setUsername] = useState("Guest");

    useEffect(() => {
        if (localStorage.getItem("auth")) {
            setIsAuth(true);
        }
        setIsLoading(false);
    }, []);

    return (
        <AuthContext.Provider
            value={{
                isAuth,
                setIsAuth,
                isLoading,
            }}
        >
            <BrowserRouter>
                <Navbar/>
                <div className="App">
                    <AppRouter/>
                </div>
                <footer
                    style={{position: 'fixed', bottom: 0}}
                >
                    2023 React study project
                </footer>
            </BrowserRouter>
        </AuthContext.Provider>
    );
}

export default App;
