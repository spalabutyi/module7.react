import axios from "axios";

export default class CertificateService {
  static async getAll(limit, page) {
    return await axios.get(`certificates`, {
      params: {
        pageSize: limit,
        pageNumber: page,
      },
    });
  }

  static async getById(id) {
    return await axios.get(`certificates/${id}`);
  }

  static async addCertificate({ name, description, price, duration }) {
    return await axios.post(
      "certificates",
      {
        name,
        description,
        price,
        duration,
      },
      { withCredentials: true }
    );
  }

  static async editCertificate(
    { name, description, price, duration },
    certificateId
  ) {
    return await axios.patch(
      `certificates/${certificateId}`,
      {
        name,
        description,
        price,
        duration,
      },
      { withCredentials: true }
    );
  }

  static async deleteCertificateById(id) {
    return await axios.delete(`certificates/${id}`, { withCredentials: true });
  }

  static async deleteCertificateTagById(certificateId, tagId) {
    return await axios.delete(`certificates/${certificateId}/tags/${tagId}`, {
      withCredentials: true,
    });
  }

  static async login(email, password) {
    return await axios.post(
      `auth/login`,
      {
        email,
        password,
      },
      { withCredentials: true }
    );
  }
}
