import React, { useMemo } from "react";

export const useSortedCertificates = (certificates, sort, direction) => {
  return useMemo(() => {
    if (sort) {
      return [...certificates].sort((a, b) => {
        return (direction === "asc" ? a[sort] > b[sort] : a[sort] < b[sort])
          ? 1
          : -1;
      });
    }
    return certificates;
  }, [sort, direction, certificates]);
};

export const useCertificates = (certificates, sort, query, direction) => {
  const sortedCertificates = useSortedCertificates(
    certificates,
    sort,
    direction
  );

  return useMemo(() => {
    return sortedCertificates.filter(
      (c) =>
        c.name.toLowerCase().includes(query) ||
        c.description.toLowerCase().includes(query)
    );
  }, [query, sortedCertificates]);
};
