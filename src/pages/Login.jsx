import React, {useContext, useState} from 'react';
import {Navigate} from 'react-router-dom';
import {AuthContext} from '../context';
import CertificateService from '../API/CertificateService';
import axios from 'axios';

const Login = () => {
  const {isAuth, setIsAuth} = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [navigate, setNavigate] = useState(false);

  const validateInputs = () => {
    let isValid = true;

    if (!email.trim()) {
      setEmailError('Email is required');
      isValid = false;
    } else if (email.length < 3 || email.length > 30) {
      setPasswordError('Email must be between 4 and 30 characters');
      isValid = false;
    } else {
      setEmailError('');
    }

    if (!password.trim()) {
      setPasswordError('Password is required');
      isValid = false;
    } else if (password.length < 4 || password.length > 30) {
      setPasswordError('Password must be between 4 and 30 characters');
      isValid = false;
    } else {
      setPasswordError('');
    }

    return isValid;
  };

  const login = async (event) => {
    event.preventDefault();

    if (!validateInputs()) {
      return;
    }

    try {
      const response = await CertificateService.login(email, password);
      if (response.data) {
        axios.defaults.headers.common['Authorization'] = `Bearer ${response.data['token']}`;
        localStorage.setItem('username', response.data.username);
        setIsAuth(true);
        setNavigate(true);
        localStorage.setItem('auth', 'true');
      } else {
        console.error('Invalid response format:', response);
      }
    } catch (e) {
      setPasswordError('Bad credentials')
    }
  };

  if (navigate) {
    return <Navigate to="/certificates"/>;
  }

  return (
      <div>
        <main className="form-signin w-100 m-auto">
          <form onSubmit={login}>
            <h1 className="h3 mb-3 fw-normal">Please sign in</h1>

            <div className="form-floating">
              <input
                  type="email"
                  className={`form-control ${emailError ? 'is-invalid' : ''}`}
                  id="floatingInput"
                  placeholder="name@example.com"
                  onChange={(e) => setEmail(e.target.value)}
              />
              <label htmlFor="floatingInput">Email address</label>
              {emailError && <div className="invalid-feedback">{emailError}</div>}
            </div>

            <div className="form-floating">
              <input
                  type="password"
                  className={`form-control ${passwordError ? 'is-invalid' : ''}`}
                  id="floatingPassword"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
              />
              <label htmlFor="floatingPassword">Password</label>
              {passwordError && <div className="invalid-feedback">{passwordError}</div>}
            </div>

            <button className="btn btn-primary w-100 py-2">Sign in</button>
          </form>
        </main>
      </div>
  );
};

export default Login;
