import React, {useEffect, useState} from "react";
import {useFetching} from "../hooks/useFetching";
import {useCertificates} from "../hooks/useCertificates";
import {getPageCount} from "../utils/pages";
import Certificate from "../components/Certificate";
import CertificateService from "../API/CertificateService";
import Loader from "../UI/Loader/Loader";
import CertificateFilter from "../components/CertificateFilter";
import {useLocation, useNavigate} from "react-router-dom";
import SimplePagination from "../UI/pagination/SimplePagination";
import SimpleSelect from "../UI/select/SimpleSelect";

const Certificates = () => {
    const location = useLocation();
    const navigate = useNavigate();
    const params = new URLSearchParams(location.search);

    const [certificates, setCertificates] = useState([]);
    const [filter, setFilter] = useState({
        sort: "",
        query: "",
        direction: "asc",
    });
    const [totalPages, setTotalPages] = useState(0);
    const [page, setPage] = useState(Number(params.get("pageNumber")) || 1);
    const [limit, setLimit] = useState(Number(params.get("pageSize")) || 10);
    const sortedAndSearchedCertificates = useCertificates(
        certificates,
        filter.sort,
        filter.query,
        filter.direction
    );

    const [fetchCertificates, isCertLoading, certError] = useFetching(
        async (limit, page) => {
            const response = await CertificateService.getAll(limit, page);
            setCertificates(response.data);
            const totalCount = response.headers["x-total-count"];
            setTotalPages(getPageCount(totalCount, limit));
        }
    );

    useEffect(() => {
        fetchCertificates(limit, page);
    }, [page, limit]);

    const handlePageSizeChange = (e) => {
        const newPageSize = Number(e.target.value);
        setLimit(newPageSize);
        updateURLParams({pageNumber: 1, pageSize: newPageSize});
    };

    const handlePageNumberChange = (newPageNumber) => {
        setPage(newPageNumber);
        updateURLParams({pageNumber: newPageNumber});
    };

    const updateURLParams = (newParams) => {
        const currentParams = new URLSearchParams(location.search);
        Object.entries(newParams).forEach(([key, value]) => {
            currentParams.set(key, value);
        });
        navigate(`${location.pathname}?${currentParams.toString()}`);
    };

    const removeCertificate = async (certificateId) => {
        try {
            const response = await CertificateService.deleteCertificateById(
                certificateId
            );
            if (response.status === 200) {
                fetchCertificates(limit, page);
                console.log("DELETE request successful");
            } else {
                console.error("DELETE request failed");
            }
        } catch (error) {
            console.error("Error during DELETE request:", error.message);
        }
    };
    const changePage = (page) => {
        setPage(page);
        fetchCertificates(limit, page);
    };

    const handleSort = (sort) => {
        let direction = "asc";
        if (filter.sort === sort && filter.direction === "asc") {
            direction = "desc";
        }
        setFilter({sort, query: "", direction});
    };

    return (
        <>
            <CertificateFilter filter={filter} setFilter={setFilter}/>
            {certError && <h1>{certError}</h1>}
            {isCertLoading ? (
                <div
                    style={{display: "flex", justifyContent: "center", marginTop: 50}}
                >
                    <Loader/>
                </div>
            ) : (
                <div></div>
            )}
            {sortedAndSearchedCertificates.length ? (
                <>
                    <h2 style={{textAlign: "center"}}>Certificate list</h2>
                    <table>
                        <thead>
                        <tr>
                            <th onClick={() => handleSort("date")}>
                                Datetime{" "}
                                {filter.sort === "date" && (
                                    <span>{filter.direction === "asc" ? "▲" : "▼"}</span>
                                )}
                            </th>
                            <th onClick={() => handleSort("name")}>
                                Title{" "}
                                {filter.sort === "name" && (
                                    <span>{filter.direction === "asc" ? "▲" : "▼"}</span>
                                )}
                            </th>
                            <th>Tags</th>
                            <th>Description</th>
                            <th onClick={() => handleSort("price")}>
                                Price{" "}
                                {filter.sort === "price" && (
                                    <span>{filter.direction === "asc" ? "▲" : "▼"}</span>
                                )}
                            </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {sortedAndSearchedCertificates.map((certificate) => (
                            <Certificate
                                {...certificate}
                                remove={removeCertificate}
                                key={certificate.id}
                            />
                        ))}
                        </tbody>
                    </table>
                </>
            ) : (
                <h2>No certificates</h2>
            )}
            <div
                style={{
                    display: "flex",
                    marginBottom: "30px",
                    justifyContent: "center",
                }}
            >
                <SimplePagination
                    page={page}
                    totalPages={totalPages}
                    changePage={changePage}
                    handlePageNumberChange={handlePageNumberChange}
                />
                <SimpleSelect
                    value={limit}
                    onChange={(newValue) => {
                        setLimit(newValue);
                        handlePageSizeChange();
                    }}
                />
            </div>
        </>
    );
};

export default Certificates;
