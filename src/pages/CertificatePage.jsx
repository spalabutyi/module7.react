import { useEffect, useState } from "react";
import { useFetching } from "../hooks/useFetching";
import {
  RiBankCard2Line,
  RiBox2Line,
  RiMoneyDollarCircleLine,
} from "react-icons/ri";
import CertificateService from "../API/CertificateService";
import Loader from "../UI/Loader/Loader";

const CertificatePage = (props) => {
  const [certificate, setCertificate] = useState({});
  const [fetchCertificateById, isLoading, error] = useFetching(async (id) => {
    const response = await CertificateService.getById(id);
    setCertificate(response.data);
  });

  useEffect(() => {
    fetchCertificateById(props.certificateId);
  }, []);

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <div
          className="modal modal-sheet position-static d-block bg-body-secondary p-4 py-md-5"
          tabIndex="-1"
          role="dialog"
          id="modalTour"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content rounded-4 shadow">
              <div className="modal-body p-5">
                <h2 className="fw-bold mb-0">{certificate.name}</h2>

                <ul className="d-grid gap-4 my-5 list-unstyled small">
                  <li className="d-flex gap-4">
                    <RiBox2Line size="40px" />
                    <div>
                      <h5 className="mb-0">{certificate.description}</h5>
                    </div>
                  </li>
                  <li className="d-flex gap-4">
                    <RiMoneyDollarCircleLine size="40px" />
                    <div>
                      <h5 className="mb-0">Price: ${certificate.price}</h5>
                      Lasts: {certificate.duration} day(s)
                    </div>
                  </li>
                  <li className="d-flex gap-4">
                    <RiBankCard2Line size="40px" />
                    <div>
                      <h5 className="mb-0">Tags: </h5>
                      {certificate.tags &&
                        certificate.tags.map((t) => (
                          <span
                            key={t.id}
                            className="badge bg-primary-subtle border border-primary-subtle text-primary-emphasis rounded-pill"
                          >
                            {t.name}
                          </span>
                        ))}
                    </div>
                  </li>
                </ul>
                <button
                  type="button"
                  className="btn btn-lg btn-primary mt-5 w-100"
                  data-bs-dismiss="modal"
                  onClick={() => props.setVisible(false)}
                >
                  Great, thanks!
                </button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default CertificatePage;
