import React, { useEffect, useState } from "react";

const About = () => {
  return (
    <div style={{ textAlign: "center" }}>
      <h2>
        <div>React study project.</div>
        <div>EPAM Java lab 2023</div>
      </h2>
    </div>
  );
};

export default About;
