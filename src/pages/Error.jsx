import React from "react";

const Error = () => {
  return (
    <div>
      <h2 style={{ textAlign: "center", color: "red" }}>404 Page not found</h2>
    </div>
  );
};

export default Error;
