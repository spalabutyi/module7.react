import { useState } from "react";
import MyInput from "../UI/input/MyInput";
import MyButton from "../UI/button/MyButton";
import CertificateService from "../API/CertificateService";

const AddCertificateForm = ({ setVisible }) => {
  const [certificate, setCertificate] = useState({
    name: "",
    description: "",
    price: 0,
    duration: 0,
  });

  const [error, setError] = useState("");

  const addNewCertificate = async (event) => {
    event.preventDefault();

    if (certificate.name.trim() === "") {
      setError("Name cannot be blank.");
      return;
    }

    if (certificate.name.length < 6 || certificate.name.length > 30) {
      setError("Name must be between 6 and 30 characters.");
      return;
    }

    if (certificate.description.trim() === "") {
      setError("Description cannot be blank.");
      return;
    }

    if (
      certificate.description.length < 12 ||
      certificate.description.length > 1000
    ) {
      setError("Description must be between 12 and 1000 characters.");
      return;
    }

    if (isNaN(certificate.price) || certificate.price <= 0) {
      setError("Price must be a number greater than 0.");
      return;
    }

    if (isNaN(certificate.duration) || certificate.duration <= 0) {
      setError("Duration must be a number greater than 0.");
      return;
    }

    try {
      const response = await CertificateService.addCertificate(certificate);

      if (response.status === 200) {
        setCertificate({
          name: "",
          description: "",
          price: 0,
          duration: 0,
        });

        setVisible(false);
        setError("");
        console.log("POST request successful");
      } else {
        setError("Error during POST request. Please try again.");
        console.error("POST request failed");
      }
    } catch (error) {
      setError(`Error during POST request: ${error.message}`);
      console.error("Error during POST request:", error.message);
    }
  };

  return (
    <form>
      <div style={{ textAlign: "center" }}>
        <h3>Add new certificate</h3>
        {error && <p style={{ color: "red" }}>{error}</p>}
      </div>
      <div>
        <label htmlFor="name">Name:</label>
        <MyInput
          id="name"
          value={certificate.name}
          onChange={(e) =>
            setCertificate({ ...certificate, name: e.target.value })
          }
          type="text"
          required
        />
      </div>
      <div>
        <label htmlFor="description">Description:</label>
        <MyInput
          id="description"
          value={certificate.description}
          onChange={(e) =>
            setCertificate({ ...certificate, description: e.target.value })
          }
          type="text"
          required
        />
      </div>
      <div>
        <label htmlFor="price">Price:</label>
        <MyInput
          id="price"
          value={certificate.price}
          onChange={(e) =>
            setCertificate({ ...certificate, price: e.target.value })
          }
          type="number"
          required
        />
      </div>
      <div>
        <label htmlFor="duration">Duration:</label>
        <MyInput
          id="duration"
          value={certificate.duration}
          onChange={(e) =>
            setCertificate({ ...certificate, duration: e.target.value })
          }
          type="number"
          required
        />
      </div>
      <MyButton type="submit" onClick={addNewCertificate}>
        Save
      </MyButton>
      <MyButton onClick={() => setVisible(false)}>Cancel</MyButton>
    </form>
  );
};

export default AddCertificateForm;
