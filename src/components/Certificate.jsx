import CertificateActions from "./CertificateActions";

const Certificate = (props) => {
  const {
    id,
    name,
    description,
    price,
    duration,
    createDate,
    tags,
    certificate,
  } = props;
  return (
    <tr>
      <th>{createDate}</th>
      <th>
        {id}: {name}
      </th>
      <th>
        {tags.length ? (
          tags.map((tag) => tag.name).join(" ")
        ) : (
          <div style={{ color: "grey" }}>No tags</div>
        )}
      </th>
      <th>{description}</th>
      <th>${price}</th>
      <th>{<CertificateActions remove={props.remove} certificateId={id} />}</th>
    </tr>
  );
};

export default Certificate;
