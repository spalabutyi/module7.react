function Tag({ name }) {
  return <span style={{ marginRight: "5px" }}>{name}</span>;
}

export default Tag;
