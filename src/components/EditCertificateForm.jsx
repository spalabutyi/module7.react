import { useEffect, useState } from "react";
import { RiCloseCircleLine } from "react-icons/ri";
import MyInput from "../UI/input/MyInput";
import MyButton from "../UI/button/MyButton";
import CertificateService from "../API/CertificateService";
import { useFetching } from "../hooks/useFetching";
import style from "./CertificateActions.module.css";

const EditCertificateForm = ({ setVisible, certificateId }) => {
  const [certificate, setCertificate] = useState({});
  const [tags, setTags] = useState([]);
  const [fetchCertificateById, isLoading, fetchError] = useFetching(
    async (id) => {
      const response = await CertificateService.getById(id);
      setCertificate(response.data);
      setTags(response.data.tags);
    }
  );

  const [error, setError] = useState("");

  useEffect(() => {
    fetchCertificateById(certificateId);
  }, [tags]);

  const editCertificate = async (event) => {
    event.preventDefault();

    if (certificate.name.trim() === "") {
      setError("Name cannot be blank.");
      return;
    }

    if (certificate.name.length < 6 || certificate.name.length > 30) {
      setError("Name must be between 6 and 30 characters.");
      return;
    }

    if (certificate.description.trim() === "") {
      setError("Description cannot be blank.");
      return;
    }

    if (
      certificate.description.length < 12 ||
      certificate.description.length > 1000
    ) {
      setError("Description must be between 12 and 1000 characters.");
      return;
    }

    if (isNaN(certificate.price) || certificate.price <= 0) {
      setError("Price must be a number greater than 0.");
      return;
    }

    if (isNaN(certificate.duration)) {
      setError("Duration must be a number.");
      return;
    }

    try {
      const response = await CertificateService.editCertificate(
        certificate,
        certificateId
      );

      if (response.status === 200) {
        setCertificate({
          name: "",
          description: "",
          price: 0,
          duration: 0,
        });

        setVisible(false);
        setError("");
        console.log("PATCH request successful");
      } else {
        setError("Error during PATCH request. Please try again.");
        console.error("PATCH request failed");
      }
    } catch (error) {
      setError(`Error during POST request: ${error.message}`);
      console.error("Error during POST request:", error.message);
    }
  };

  const deleteTag = async (certificateId, tagId) => {
    const confirm = window.confirm("Delete tag");
    if (confirm) {
      const response = await CertificateService.deleteCertificateTagById(
        certificateId,
        tagId
      );
      if (response.status === 200) {
        setTags(tags.filter((tag) => tag.id !== tagId));
        setVisible(true);
      }
    }
  };

  return (
    <form>
      <div style={{ textAlign: "center" }}>
        <h3>Edit certificate with id = {certificateId}</h3>
        {error && <p style={{ color: "red" }}>{error}</p>}
      </div>
      <div>
        <label htmlFor="name">Name:</label>
        <MyInput
          id="name"
          value={certificate.name}
          onChange={(e) =>
            setCertificate({ ...certificate, name: e.target.value })
          }
          type="text"
          required
        />
      </div>
      <div>
        <label htmlFor="description">Description:</label>
        <MyInput
          id="description"
          value={certificate.description}
          onChange={(e) =>
            setCertificate({ ...certificate, description: e.target.value })
          }
          type="text"
          required
        />
      </div>
      <div>
        <label htmlFor="price">Price:</label>
        <MyInput
          id="price"
          value={certificate.price}
          onChange={(e) =>
            setCertificate({ ...certificate, price: e.target.value })
          }
          type="number"
          required
        />
      </div>
      <div>
        <label htmlFor="duration">Duration:</label>
        <MyInput
          id="duration"
          value={certificate.duration}
          onChange={(e) =>
            setCertificate({ ...certificate, duration: e.target.value })
          }
          type="number"
          required
        />
        <div>
          <h5 className="mb-0">Tags: </h5>
          {tags &&
            tags.map((tag) => (
              <span
                key={tag.id}
                className="badge bg-warning-subtle border border-warning-subtle text-warning-emphasis rounded-pill"
              >
                {tag.name}{" "}
                <RiCloseCircleLine
                  className={style.deleteTagIcon}
                  onClick={() => deleteTag(certificateId, tag.id)}
                />
              </span>
            ))}
        </div>
      </div>
      <MyButton type="submit" onClick={editCertificate}>
        Save
      </MyButton>
      <MyButton onClick={() => setVisible(false)}>Cancel</MyButton>
    </form>
  );
};

export default EditCertificateForm;
