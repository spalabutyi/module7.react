import { RiDeleteBin2Line, RiEdit2Fill, RiEyeLine } from "react-icons/ri";
import styles from "./CertificateActions.module.css";
import { useState } from "react";
import CertificateModal from "../UI/Modal/CertificateModal";
import CertificatePage from "../pages/CertificatePage";
import DeleteCertificate from "./DeleteCertificate";
import EditCertificateForm from "./EditCertificateForm";

const CertificateActions = (props) => {
  const [watchModalVisible, setWatchModalVisible] = useState(false);
  const [editModalVisible, setEditModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);

  return (
    <div className={styles.actionContainer}>
      <RiEyeLine
        className={styles.watchIcon}
        onClick={() => setWatchModalVisible(true)} // Corrected state variable
      />
      <RiEdit2Fill
        className={styles.editIcon}
        onClick={() => setEditModalVisible(true)}
        certificateId={props.certificateId}
      />
      <RiDeleteBin2Line
        className={styles.deleteIcon}
        onClick={() => setDeleteModalVisible(true)}
      />

      {watchModalVisible && (
        <CertificateModal
          visible={watchModalVisible}
          setVisible={setWatchModalVisible}
        >
          <CertificatePage
            setVisible={setWatchModalVisible}
            certificateId={props.certificateId}
          />
        </CertificateModal>
      )}

      {editModalVisible && (
        <CertificateModal
          visible={editModalVisible}
          setVisible={setEditModalVisible}
        >
          <EditCertificateForm
            setVisible={setEditModalVisible}
            certificateId={props.certificateId}
            certificate={props.certificate}
          />
        </CertificateModal>
      )}

      {deleteModalVisible && (
        <CertificateModal
          visible={deleteModalVisible}
          setVisible={setDeleteModalVisible}
        >
          <DeleteCertificate
            setVisible={setDeleteModalVisible}
            certificateId={props.certificateId}
            remove={props.remove}
          />
        </CertificateModal>
      )}
    </div>
  );
};

export default CertificateActions;
