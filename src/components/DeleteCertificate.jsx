import React from "react";

const DeleteCertificate = ({ setVisible, certificateId, remove }) => {
  return (
    <div
      className="modal modal-sheet position-static d-block bg-body-secondary p-4 py-md-5"
      tabIndex="-1"
      role="dialog"
      id="modalChoice"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content rounded-3 shadow">
          <div className="modal-body p-4 text-center">
            <h5 className="mb-0">Delete confirmation</h5>
            <p className="mb-0">
              Do you really want to delete certificate with id = {certificateId}
            </p>
          </div>
          <div className="modal-footer flex-nowrap p-0">
            <button
              type="button"
              className="btn btn-lg btn-link fs-6 text-decoration-none col-6 py-3 m-0 rounded-0 border-end"
              onClick={() => remove(certificateId)}
            >
              <strong>Yes</strong>
            </button>
            <button
              type="button"
              className="btn btn-lg btn-link fs-6 text-decoration-none col-6 py-3 m-0 rounded-0"
              data-bs-dismiss="modal"
              onClick={() => setVisible(false)}
            >
              No thanks
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DeleteCertificate;
