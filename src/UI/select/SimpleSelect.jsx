import React from "react";
import style from "./SimpleSelect.module.css";

const MySelect = ({ onChange, value }) => {
  return (
    <div>
      <select
        className={style.selector}
        id="pageSize"
        value={value}
        onChange={(event) => onChange(event.target.value)}
      >
        <option value="10">10</option>
        <option value="20">20</option>
        <option value="50">50</option>
      </select>
    </div>
  );
};

export default MySelect;
