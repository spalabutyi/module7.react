import React from "react";
import styles from "./CertificateModal.module.css";

const CertificateModal = ({ children, visible, setVisible }) => {
  const rootClass = [styles.myModal];
  if (visible) {
    rootClass.push(styles.active);
  }

  return (
    <div className={rootClass.join(" ")} onClick={() => setVisible(false)}>
      <div
        className={styles.myModalContent}
        onClick={(e) => e.stopPropagation()}
      >
        {children}
      </div>
    </div>
  );
};

export default CertificateModal;
