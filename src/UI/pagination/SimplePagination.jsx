import React from "react";
import { getLimitedPagesArray } from "../../utils/pages";
import styles from "./SimplePagination.module.css";

const Pagination = ({ totalPages, page, handlePageNumberChange }) => {
  let pagesArray = getLimitedPagesArray(totalPages, page);

  return (
    <div className={styles.page__wrapper}>
      <span
        className={styles.page}
        style={{ marginRight: "10px" }}
        onClick={() => handlePageNumberChange(1)}
        disabled={page === 1}
      >
        First
      </span>
      <span
        className={styles.page}
        style={{ marginRight: "10px" }}
        onClick={() => handlePageNumberChange(page - 1)}
        disabled={page === 1}
      >
        Previous
      </span>
      {pagesArray.map((p) => (
        <span
          onClick={() => handlePageNumberChange(p)}
          key={p}
          className={page === p ? styles.page__current : styles.page}
        >
          {p}
        </span>
      ))}
      <span
        onClick={() => handlePageNumberChange(page + 1)}
        className={styles.page}
        style={{ marginRight: "10px" }}
      >
        Next
      </span>
      <span
        className={styles.page}
        style={{ marginLeft: "10px" }}
        onClick={() => handlePageNumberChange(totalPages)}
        disabled={page === totalPages}
      >
        Last
      </span>
    </div>
  );
};

export default Pagination;
