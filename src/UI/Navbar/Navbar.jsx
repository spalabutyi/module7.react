import { Link } from "react-router-dom";
import MyButton from "../button/MyButton";
import { useContext, useState } from "react";
import { AuthContext } from "../../context";
import styles from "./Navbar.module.css";
import CertificateModal from "../Modal/CertificateModal";
import AddCertificateForm from "../../components/AddCertificateForm";

const Navbar = ({ onAddButtonClick }) => {
  const { isAuth, setIsAuth } = useContext(AuthContext);
  const username = localStorage.getItem("username");
  const [modal, setModal] = useState(false);
  const logout = () => {
    setIsAuth(false);
    localStorage.removeItem("username");
    localStorage.removeItem("auth");
  };

  return (
    <div className={styles.navbar}>
      <div className={styles.navbar__btn}>
        <MyButton onClick={logout}>Logout</MyButton>
      </div>
      {isAuth ? (
        <div className={styles.navbar__btn}>
          <MyButton onClick={() => setModal(true)}>Add new</MyButton>
        </div>
      ) : (
        <></>
      )}

      <div className={styles.navbar__links}>
        <Link to="/certificates" className={styles.links}>
          Certificates
        </Link>
        <Link to="/about" className={styles.links}>
          About
        </Link>
        <strong className={styles.links}>{username}</strong>
      </div>
      <div>
        <CertificateModal visible={modal} setVisible={setModal}>
          <AddCertificateForm setVisible={setModal} />
        </CertificateModal>
      </div>
    </div>
  );
};

export default Navbar;
